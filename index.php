<?php

require_once  './vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;

/**
 * Initialize Cloud Firestore with default project ID.
 * ```
 * initialize();
 * ```
 */
function initialize()
{
    // Create the Cloud Firestore client
    $db = new FirestoreClient();
    die(print_r('Created Cloud Firestore client with default project ID.' . PHP_EOL));
}

initialize();



?>
